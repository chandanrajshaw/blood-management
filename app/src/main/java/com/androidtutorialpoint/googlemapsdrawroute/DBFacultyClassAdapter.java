package com.androidtutorialpoint.googlemapsdrawroute;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.androidtutorialpoint.googlemapsdrawroute.DBFacultyClassAdapter.DataBaseClassFaculty.TABLE_NAME;



/**
 * Created by nareshit on 22-Sep-16.
 */
public class DBFacultyClassAdapter {
    DataBaseClassFaculty dataBaseClass ;
    public DBFacultyClassAdapter(Context context)
    {
         dataBaseClass=new DataBaseClassFaculty(context);
    }
    public  long insertData(String Name , String City, String Contactno , String BloodGroup, String AgeGroup, String Address)
    {
        SQLiteDatabase sqLiteDatabase=dataBaseClass.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(DataBaseClassFaculty.NAME,Name);
        contentValues.put(DataBaseClassFaculty.CITY,City);
        contentValues.put(DataBaseClassFaculty.CONTACTNO,Contactno);
        contentValues.put(DataBaseClassFaculty.BLOODGROUP,BloodGroup );
        contentValues.put(DataBaseClassFaculty.AGEDROUP, AgeGroup);
        contentValues.put(DataBaseClassFaculty.ADDRESS,Address);

        long id=sqLiteDatabase.insert(TABLE_NAME,null,contentValues);
        return id;
    }

    //==========================================================================

    public String getData(String City, String BloodGroup ) {

        SQLiteDatabase sqLiteDatabase = dataBaseClass.getWritableDatabase();
        String[] columns = {DataBaseClassFaculty.NAME, DataBaseClassFaculty.CONTACTNO, DataBaseClassFaculty.ADDRESS, DataBaseClassFaculty.CITY, DataBaseClassFaculty.AGEDROUP, DataBaseClassFaculty.BLOODGROUP};

        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, columns,"City = ? AND BloodGroup = ?" , new String[]{City,BloodGroup}, null, null, null);
        StringBuffer stringBuffer = new StringBuffer();
        if (cursor.moveToNext())
        {
            int index1 = cursor.getColumnIndex(DataBaseClassFaculty.NAME);
            int index4 = cursor.getColumnIndex(DataBaseClassFaculty.CITY);
            int index2 = cursor.getColumnIndex(DataBaseClassFaculty.CONTACTNO);
            int index3 = cursor.getColumnIndex(DataBaseClassFaculty.BLOODGROUP);
            int index6 = cursor.getColumnIndex(DataBaseClassFaculty.ADDRESS);
            int index5 = cursor.getColumnIndex(DataBaseClassFaculty.AGEDROUP);
            String getname = cursor.getString(index1);
            String getcity = cursor.getString(index4);
            String getpno = cursor.getString(index2);
            String getaddress = cursor.getString(index6);
            String getbgroup = cursor.getString(index3);
            String getagegroup = cursor.getString(index5);

            stringBuffer.append("Name:- "+getname+"\nCity:- "+getcity+"\nContact no:- "+getpno+"\nAddress:- "+getaddress+"\nBlood Group:-"+getbgroup+"\nAge Group:-"+getagegroup) ;
        }
        return stringBuffer.toString();
    }
    //================================================================================================================
 //to update detail

    public  int updateData(String Name, String City, String oldContactDetail, String Address) {
        SQLiteDatabase sqLiteDatabase = dataBaseClass.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseClassFaculty.NAME, Name);
        contentValues.put(DataBaseClassFaculty.CITY, City);
        contentValues.put(DataBaseClassFaculty.CONTACTNO, oldContactDetail);
        contentValues.put(DataBaseClassFaculty.ADDRESS, Address);
        int i = sqLiteDatabase.update(TABLE_NAME, contentValues, DataBaseClassFaculty.CONTACTNO + " =?", new String[]{oldContactDetail});
        return i;
    }
    //==========================================================================
    //get user
    public String showAllAssigendUserAdmin(){
        SQLiteDatabase sqLiteDatabase=dataBaseClass.getWritableDatabase();

        String[] columns={DataBaseClassFaculty.NAME, DataBaseClassFaculty.CONTACTNO, DataBaseClassFaculty.ADDRESS, DataBaseClassFaculty.CITY, DataBaseClassFaculty.AGEDROUP, DataBaseClassFaculty.BLOODGROUP};

        Cursor cursor= sqLiteDatabase.query(TABLE_NAME,columns,null,null,null,null,null) ;
        StringBuffer stringBuffer=new StringBuffer();

        while (cursor.moveToNext()) {
            int index1 = cursor.getColumnIndex(DataBaseClassFaculty.NAME);
            int index4 = cursor.getColumnIndex(DataBaseClassFaculty.CITY);
            int index2 = cursor.getColumnIndex(DataBaseClassFaculty.CONTACTNO);
            int index3 = cursor.getColumnIndex(DataBaseClassFaculty.BLOODGROUP);
            int index6 = cursor.getColumnIndex(DataBaseClassFaculty.ADDRESS);
            int index5 = cursor.getColumnIndex(DataBaseClassFaculty.AGEDROUP);
            String getname = cursor.getString(index1);
            String getcity = cursor.getString(index4);
            String getpno = cursor.getString(index2);
            String getaddress = cursor.getString(index6);
            String getbgroup = cursor.getString(index3);
            String getagegroup = cursor.getString(index5);
            stringBuffer.append("\nName:- "+getname+"\nCity:- "+getcity+"\nContact no:- "+getpno+"\nAddress:- "+getaddress+"\nBlood Group:-"+getbgroup+"\nAge Group:-"+getagegroup) ;
        }
        return stringBuffer.toString();

    }
    public String ShowotherList(){
        SQLiteDatabase sqLiteDatabase=dataBaseClass.getReadableDatabase();
        String[] columns={DataBaseClassFaculty.NAME, DataBaseClassFaculty.CONTACTNO, DataBaseClassFaculty.ADDRESS, DataBaseClassFaculty.CITY, DataBaseClassFaculty.AGEDROUP, DataBaseClassFaculty.BLOODGROUP};
        Cursor cursor= sqLiteDatabase.query(TABLE_NAME,columns,null,null,null,null,null) ;
        StringBuffer stringBuffer=new StringBuffer();
        while (cursor.moveToNext()) {
            int index1 = cursor.getColumnIndex(DataBaseClassFaculty.NAME);
            int index4 = cursor.getColumnIndex(DataBaseClassFaculty.CITY);
            int index2 = cursor.getColumnIndex(DataBaseClassFaculty.CONTACTNO);
            int index3 = cursor.getColumnIndex(DataBaseClassFaculty.BLOODGROUP);
            int index6 = cursor.getColumnIndex(DataBaseClassFaculty.ADDRESS);
            int index5 = cursor.getColumnIndex(DataBaseClassFaculty.AGEDROUP);
            String getname = cursor.getString(index1);
            String getcity = cursor.getString(index4);
            String getpno = cursor.getString(index2);
            String getaddress = cursor.getString(index6);
            String getbgroup = cursor.getString(index3);
            String getagegroup = cursor.getString(index5);
            stringBuffer.append("Name:- "+getname+"\nCity:- "+getcity+"\nContact no:- "+getpno+"\nAddress:- "+getaddress+"\nBlood Group:-"+getbgroup+"\nAge Group:-"+getagegroup+"\n") ;
        }
        return stringBuffer.toString();
    }
    //================================================================================================================
    static class DataBaseClassFaculty  extends SQLiteOpenHelper
    {
        private static final String DATABASE_NAME="DONATER";
        private static final int DATABASE_VERSION=1;
        static final String TABLE_NAME="DONATERDETAIL";
        private static final String UID="_id";
        private static final String NAME="Name";
        private static final String CITY="City";
        private static final String CONTACTNO="Contactno";
        private static final String BLOODGROUP="BloodGroup";
        private static final String AGEDROUP="AgeGroup";
        private static final String ADDRESS="Address";

        private static final String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+"(" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+NAME+" VARCHAR(255),"+CITY+" VARCHAR(255),"+CONTACTNO+" VARCHAR(255),"+BLOODGROUP+" VARCHAR(255),"+AGEDROUP+" VARCHAR(255),"+ADDRESS+" VARCHAR(255));";

        private static final String DROP_TABLE= "DROP TABLE IF EXISTS " + TABLE_NAME;
        private Context context;


        public DataBaseClassFaculty(Context context) {
            super(context,DATABASE_NAME, null, DATABASE_VERSION);
            this.context=context;
          //  Message.message(context, "constructor is called");
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {


            try  {
                sqLiteDatabase.execSQL(CREATE_TABLE);
              //  Message.message(context, "onCreate  is called");
             }
            catch (SQLException e)
            {
                Message.message(context, ""+e);


            }

        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            //DELETE TABLE IF EXIST "P_INFO_TABLE" ;
             //  String DROP_TABLE= "DROP TABLE IF EXIST"+TABLE_NAME;

            try  {
                sqLiteDatabase.execSQL(DROP_TABLE);
                onCreate(sqLiteDatabase);
            //   Message.message(context, "onUpgrade is called");

            }
            catch (SQLException e)
            {
                Message.message(context, ""+e);
            }

        }
    }

}

