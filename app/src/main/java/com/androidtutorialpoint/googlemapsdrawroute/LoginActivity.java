package com.androidtutorialpoint.googlemapsdrawroute;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_login;
    TextView tv_signup, tv_forgetpwd;
    EditText edt_mobileNumber, edt_password;
    String mobile, password, mobile_number, pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("Register", Context.MODE_PRIVATE);
        mobile = sharedPref.getString("mobile", "mobile");
        password = sharedPref.getString("pwd", "pwd");

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        btn_login = (Button) findViewById(R.id.btn_login);
        tv_signup = (TextView) findViewById(R.id.tv_signup);
        edt_mobileNumber = (EditText) findViewById(R.id.edt_mobileNumber);
        edt_password = (EditText) findViewById(R.id.edt_password);
        tv_forgetpwd = (TextView) findViewById(R.id.tv_forgetpwd);

        tv_signup.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        tv_forgetpwd.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                mobile_number = edt_mobileNumber.getText().toString().trim();
                pwd = edt_password.getText().toString().trim();
                if (mobile_number.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Mobile Number Should not be Empty", Toast.LENGTH_SHORT).show();
                } else if (pwd.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Password Should not be Empty", Toast.LENGTH_SHORT).show();
                } else if (mobile_number.equals(mobile) & pwd.equals(password)) {
                    Intent i = new Intent(LoginActivity.this, SearchActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Mobile Number or Password", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_signup:
                Intent in = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(in);
                break;
            case R.id.tv_forgetpwd:
                String mobile_number = edt_mobileNumber.getText().toString().trim();
                if (mobile_number.equals(mobile)) {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(mobile_number, null, "Your Password is : " + password, null, null);
                    Toast.makeText(getApplicationContext(), "SMS Sent!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter a valid Mobile Number", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

}
