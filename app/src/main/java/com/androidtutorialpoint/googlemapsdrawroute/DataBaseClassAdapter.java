package com.androidtutorialpoint.googlemapsdrawroute;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DataBaseClassAdapter
{
    DataBaseClass dataBaseClass ;


    public DataBaseClassAdapter(Context context)
    {
        dataBaseClass=new DataBaseClass(context);

    }
    //for showing data  for main class
    public  boolean isAuthenticated(String Name, String Password){
        SQLiteDatabase sqLiteDatabase= dataBaseClass.getWritableDatabase();
        String[] columns={DataBaseClass.NAME, DataBaseClass.PASSWORD};
        String[] selectionArgs={Name,Password};
        Cursor cursor= sqLiteDatabase.query
                (DataBaseClass.TABLE_NAME,columns,"Name=? And Password=?",new String[]{Name,Password},null,null,null) ;
        Boolean f=false;

        if (cursor.moveToNext()) {
          f=true;
        }
        return f;
    }

    //=======================================================================

    public String showpassword(String Name)
    {
        SQLiteDatabase sqLiteDatabase;
        sqLiteDatabase = dataBaseClass.getWritableDatabase();
        String[] columns={DataBaseClass.NAME, DataBaseClass.PASSWORD};

        Cursor cursor= sqLiteDatabase.query(DataBaseClassAdapter.DataBaseClass.TABLE_NAME,columns,"Name=?",new String[]{Name},null,null,null);
        StringBuffer stringBuffer=new StringBuffer();
        while (cursor.moveToNext()) {
            int indexpwd = cursor.getColumnIndex(DataBaseClass.PASSWORD);
            String get_pwd_from_db=cursor.getString(indexpwd);
            stringBuffer.append("Password: " + get_pwd_from_db) ;
        }
        return stringBuffer.toString();
    }
    //===================================================

 public long insert(String Name, String Password, String CPassword)
 {

        SQLiteDatabase sqLiteDatabase=dataBaseClass.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(DataBaseClass.NAME,Name);
        contentValues.put(DataBaseClass.PASSWORD,Password);
        contentValues.put(DataBaseClass.CPASSWORD,CPassword);


     long id=sqLiteDatabase.insert(DataBaseClass.TABLE_NAME,null,contentValues);
        return id;

    }

    //=======================================================================

    static class DataBaseClass  extends SQLiteOpenHelper
    {


        private static final String DATABASE_NAME="REGISTER";
        private static final String TABLE_NAME="REGISTER_PEOPLE";
        private static final String UID="_id";
        private static final int DATABASE_VERSION=1;
        private static final String NAME="Name";
        private static final String PASSWORD="Password";
        private static final String CPASSWORD="CPassword";



        private static final String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+"(" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+NAME+" VARCHAR(255),"+PASSWORD+" VARCHAR(255),"+CPASSWORD+" VARCHAR(255));";

        private static final String DROP_TABLE= "DROP TABLE IF EXISTS " + TABLE_NAME;
        private Context context;


        public DataBaseClass(Context context) {
            super(context,DATABASE_NAME, null, DATABASE_VERSION);
            this.context=context;
         //   Message.message(context, "constructor is called");
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            try  {
                sqLiteDatabase.execSQL(CREATE_TABLE);
             //  Message.message(context, "onCreate  is called");
             }
            catch (SQLException e)
            {
                Message.message(context, ""+e);
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            //DELETE TABLE IF EXIST "P_INFO_TABLE" ;
             //  String DROP_TABLE= "DROP TABLE IF EXIST"+TABLE_NAME;
            try  {
                sqLiteDatabase.execSQL(DROP_TABLE);
                onCreate(sqLiteDatabase);
             //   Message.message(context, "onUpgrade is called");

            }
            catch (SQLException e)
            {
                Message.message(context, ""+e);
            }
        }
    }

}

