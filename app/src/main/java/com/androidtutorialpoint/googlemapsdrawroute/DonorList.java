package com.androidtutorialpoint.googlemapsdrawroute;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DonorList extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ListView listView;
    Button button, button1;
    DBFacultyClassAdapter dataBaseClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donor_list);
        dataBaseClass = new DBFacultyClassAdapter(this);
        // button= (Button) findViewById(R.id.button8);
        String data = dataBaseClass.showAllAssigendUserAdmin();
        String[] mydata = data.split("#");
        listView = (ListView) findViewById(R.id.list);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mydata);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
    }
    public void findlocation(View view){
        Intent intent=new Intent(DonorList.this,SearchActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
        TextView temp = (TextView) view;

        Toast.makeText(getApplicationContext(), temp.getText() + "" + i, Toast.LENGTH_LONG).show();

    }
}
