package com.androidtutorialpoint.googlemapsdrawroute;
//this class is not used anywhere

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edt_Fname, edt_Lname, edt_mobileNumber, edt_pwd, edt_reenterpwd;
    Button btn_signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        edt_Fname = (EditText) findViewById(R.id.edt_Fname);
        edt_Lname = (EditText) findViewById(R.id.edt_Lname);
        edt_mobileNumber = (EditText) findViewById(R.id.edt_mobileNumber);
        edt_pwd = (EditText) findViewById(R.id.edt_pwd);
        edt_reenterpwd = (EditText) findViewById(R.id.edt_reenterpwd);
        btn_signUp = (Button) findViewById(R.id.btn_signUp);

        btn_signUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signUp:
                String Fname = edt_Fname.getText().toString().trim();
                String Lname = edt_Lname.getText().toString().trim();
                String mobile = edt_mobileNumber.getText().toString().trim();
                String pwd = edt_pwd.getText().toString().trim();
                String re_pwd = edt_reenterpwd.getText().toString().trim();

                if (Fname.length() == 0) {
                    Toast.makeText(getApplicationContext(), "First Name Should Not be Empty", Toast.LENGTH_SHORT).show();
                } else if (Lname.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Last Name Should Not be Empty", Toast.LENGTH_SHORT).show();
                } else if (mobile.length() == 0 & mobile.length() > 10) {
                    Toast.makeText(getApplicationContext(), "Please Check Your Mobile Number", Toast.LENGTH_SHORT).show();
                } else if (pwd.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Password Should Not be Empty", Toast.LENGTH_SHORT).show();
                } else if (re_pwd.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Re-Enter Password Should Not be Empty", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("Register", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("Fname", Fname);
                    editor.putString("Lname", Lname);
                    editor.putString("mobile", mobile);
                    editor.putString("pwd", pwd);
                    editor.putString("re_pwd", re_pwd);
                    editor.commit();
                    Intent i = new Intent(SignupActivity.this, LoginActivity.class);
                    startActivity(i);
                }
                break;
        }
    }
}
