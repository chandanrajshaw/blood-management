package com.androidtutorialpoint.googlemapsdrawroute;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class OthersList extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    EditText donname,doncity,doncontact,donaddress;
    ArrayAdapter donadapter_gender,donadapter_age;
    Spinner donspinner_bgroup,donspinner_agegroup;
    String  donselected_gender,donselected_age;
    String name,city,contact,address;
    DBFacultyClassAdapter dataBaseClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others_list);
        donname=(EditText)findViewById(R.id.donname);
        doncity=(EditText)findViewById(R.id.doncity);
        doncontact=(EditText)findViewById(R.id.doncontcat);
        donaddress=(EditText)findViewById(R.id.donadress);
        donspinner_bgroup= (Spinner) findViewById(R.id.donspinner);
        donspinner_agegroup= (Spinner) findViewById(R.id.donspinner2);

        dataBaseClass = new DBFacultyClassAdapter(this);
        donadapter_gender = ArrayAdapter.createFromResource(this, R.array.Blood_Group, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        donadapter_gender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        donspinner_bgroup.setAdapter(donadapter_gender);


        //Array adapter for age spinner class
        donadapter_age = ArrayAdapter.createFromResource(this,
                R.array.Age_Group, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        donadapter_age.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        donspinner_agegroup.setAdapter(donadapter_age);



    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String label = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        // Toast.makeText(parent.getContext(), "You selected: " + label,Toast.LENGTH_LONG).show();


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public void Donsave(View view){

        donselected_gender  = donspinner_bgroup.getSelectedItem().toString();
        donselected_age  = donspinner_agegroup.getSelectedItem().toString();

        name=donname.getText().toString().trim();
        if (!StringUtils.isValidString(name)) {
            Toast.makeText(getApplicationContext(), "Enter valid name", Toast.LENGTH_LONG).show();
            return;
        }
        city=doncity.getText().toString().trim();
        if (!StringUtils.isValidString(city)) {
            Toast.makeText(getApplicationContext(), "Enter city name", Toast.LENGTH_LONG).show();
            return;
        }
        contact=doncontact.getText().toString().trim();
        if (!StringUtils.isValidString(contact)) {
            Toast.makeText(getApplicationContext(), "Enter  password", Toast.LENGTH_LONG).show();
            return;
        }

        address=donaddress.getText().toString().trim();
        if (!StringUtils.isValidString(address)) {
            Toast.makeText(getApplicationContext(), "Enter  cpwd", Toast.LENGTH_LONG).show();
            return;
        }


        //  Toast.makeText(this, "You selected: " + name+city+contact+address+selected_gender+selected_gender+selected_age, Toast.LENGTH_LONG).show();


        long id= dataBaseClass.insertData(name,city,contact,donselected_gender,donselected_age,address) ;
        if (id<0){
            Message.message(this,"unsuccessfull");
        }
        else {
            Message.message(this,"successfully insert a row");
            Intent intent=new Intent(getApplicationContext(),SigninPage.class);
            startActivity(intent);

        }
    }
    public  void donupdate(View view) {
        name=donname.getText().toString().trim();
        if (!StringUtils.isValidString(name)) {
            Toast.makeText(getApplicationContext(), "Enter valid name", Toast.LENGTH_LONG).show();
            return;
        }
        city=doncity.getText().toString().trim();
        if (!StringUtils.isValidString(city)) {
            Toast.makeText(getApplicationContext(), "Enter city name", Toast.LENGTH_LONG).show();
            return;
        }
        contact=doncontact.getText().toString().trim();
        if (!StringUtils.isValidString(contact)) {
            Toast.makeText(getApplicationContext(), "Enter  password", Toast.LENGTH_LONG).show();
            return;
        }

        address=donaddress.getText().toString().trim();
        if (!StringUtils.isValidString(address)) {
            Toast.makeText(getApplicationContext(), "Enter  cpwd", Toast.LENGTH_LONG).show();
            return;
        }

        //updating detail
        int update=dataBaseClass.updateData( name, city, contact, address);

        if(update>=0)
        {
            Toast.makeText(getApplicationContext(), "Data updated",Toast.LENGTH_LONG).show();

        }
        else
        {
            Toast.makeText(getApplicationContext(), "Data not updated",Toast.LENGTH_LONG).show();

        }


    }
}

