package com.androidtutorialpoint.googlemapsdrawroute;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ForgotPassword extends AppCompatActivity {
    EditText editText;
    TextView textView;
    Button btn_getpwd;
    DataBaseClassAdapter dataBaseClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        editText= (EditText) findViewById(R.id.repwd);
        textView= (TextView) findViewById(R.id.frpwd);
        textView.setVisibility(View.GONE);
        btn_getpwd= (Button) findViewById(R.id.button9);
        dataBaseClass=new DataBaseClassAdapter(this);

    }
   //------------------------------forget password--------------------------------------

    public  void getpwd(View view){
        String name=  editText.getText().toString();
        String data=  dataBaseClass.showpassword(name);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent intent = new Intent(ForgotPassword.this, SigninPage.class);//on clicking notification
        PendingIntent pIntent = PendingIntent.getActivity(ForgotPassword.this, 0, intent, 0);
        Notification mNotification = new Notification.Builder(this)
                .setContentTitle("Notification for Forget Password")
                .setContentText("Your password is :-"+""+data)
                .setSmallIcon(R.drawable.ninja)
                .setContentIntent(pIntent)
                .setSound(soundUri)
                .addAction(R.drawable.ninja, "View", pIntent)
                .addAction(0, "Remind", pIntent)
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, mNotification);


    }
}
