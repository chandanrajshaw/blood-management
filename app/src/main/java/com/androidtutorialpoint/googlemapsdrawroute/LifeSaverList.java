package com.androidtutorialpoint.googlemapsdrawroute;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
public class LifeSaverList extends AppCompatActivity implements AdapterView.OnItemClickListener{
    ListView listView;
    Button button, button1;
    DBFacultyClassAdapter dataBaseClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_saver_list);
        dataBaseClass = new DBFacultyClassAdapter(this);
        String data = dataBaseClass.ShowotherList();

        String[] mydata = data.split("#\n");
        listView = (ListView) findViewById(R.id.list1);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mydata);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
    }
    public void finddonlocation(View view){
        Intent intent=new Intent(LifeSaverList.this,SearchActivity.class);
        startActivity(intent);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
        TextView temp = (TextView) view;

        Toast.makeText(getApplicationContext(), temp.getText() + "" + i, Toast.LENGTH_LONG).show();
    }
}
