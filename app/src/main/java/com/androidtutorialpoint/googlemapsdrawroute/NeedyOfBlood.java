package com.androidtutorialpoint.googlemapsdrawroute;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
public class NeedyOfBlood extends AppCompatActivity implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener{
        EditText editText_area;
        ArrayAdapter adapter_bg;
        Spinner spinner_bg;
        DBFacultyClassAdapter dataBaseClass;
        ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_needy_of_blood);
        editText_area=(EditText)findViewById(R.id.editText6);
        spinner_bg= (Spinner) findViewById(R.id.spinner3);
        dataBaseClass = new DBFacultyClassAdapter(this);
        adapter_bg = ArrayAdapter.createFromResource(this,
                R.array.Blood_Group, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_bg.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_bg.setAdapter(adapter_bg);
    }
    public void FinddonLocation(View view){
        Intent intent=new Intent(NeedyOfBlood.this,SearchActivity.class);
        startActivity(intent);
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String label = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        // Toast.makeText(parent.getContext(), "You selected: " + label, Toast.LENGTH_LONG).show();
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void find(View view){
        String area=editText_area.getText().toString().trim();
        String selected_bg = spinner_bg.getSelectedItem().toString();
        Toast.makeText(this,area+selected_bg,Toast.LENGTH_LONG).show();
        String data = dataBaseClass.getData(area,selected_bg);
        Toast.makeText(getApplicationContext(),data, Toast.LENGTH_SHORT).show();
        if (data != null) {
            String[] mydata = data.split(" \n");
            listView= (ListView) findViewById(R.id.list);
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mydata);
            listView.setAdapter(arrayAdapter);
            listView.setOnItemClickListener(this);
        } else {
            Toast.makeText(getApplicationContext(), "enter correct phoneno",
                    Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
