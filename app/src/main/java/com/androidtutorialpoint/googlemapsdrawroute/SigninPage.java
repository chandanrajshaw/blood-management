package com.androidtutorialpoint.googlemapsdrawroute;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SigninPage extends AppCompatActivity {
    EditText et_fullname, et_rollno;
    Button blogin,sign_up,forgotPwd;
    DataBaseClassAdapter dataBaseClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin_page);
        et_fullname = (EditText) findViewById(R.id.editText7);
        et_rollno = (EditText) findViewById(R.id.editText8);
        blogin = (Button) findViewById(R.id.button7);
        forgotPwd = (Button) findViewById(R.id.button4);
        sign_up = (Button) findViewById(R.id.button22);
        et_fullname.requestFocus();
        dataBaseClass = new DataBaseClassAdapter(this);
    }
    public void signUP(View view){
        Intent intent=new Intent(this,SignUp.class);
        startActivity(intent);
    }
    public void forgotPassword(View view){
        Intent intent=new Intent(this,ForgotPassword.class);
        startActivity(intent);
    }
    public void myLogin(View view) {
        String enteredfullname = et_fullname.getText().toString().trim();
        if (!StringUtils.isValidString(enteredfullname)) {
            Toast.makeText(getApplicationContext(), "Enter valid name", Toast.LENGTH_LONG).show();
            return;
        }
        String enteredrollno = et_rollno.getText().toString().trim();
        if (!StringUtils.isValidString(enteredrollno)) {
            Toast.makeText(getApplicationContext(), "Enter city name", Toast.LENGTH_LONG).show();
            return;
        }
        if (enteredfullname.equalsIgnoreCase("admin") & enteredrollno.equalsIgnoreCase("admin")) {
            Toast.makeText(this, "Username and password is correct",
                    Toast.LENGTH_SHORT).show();
            Intent intentadmin = new Intent(this, AdminPage.class);
            startActivity(intentadmin);
        }
        else {
            boolean f=  dataBaseClass.isAuthenticated(enteredfullname, enteredrollno);
            if (f) {
//                SharedPreferences sharedPreferences=getSharedPreferences("Login",)
                Toast.makeText(this, "Successfully Login", Toast.LENGTH_SHORT).show();
                Intent intentadmin = new Intent(getApplicationContext(), HomePage.class);
                startActivity(intentadmin);
                finish();
            }
            else {
                Toast.makeText(getApplicationContext(),"Username Or Password is Incorrect",Toast.LENGTH_SHORT).show();
            }
        }
    }
}

