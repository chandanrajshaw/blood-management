package com.androidtutorialpoint.googlemapsdrawroute;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class HomePage extends AppCompatActivity {
    ImageButton give_blood,take_blood;
    Button Others;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        give_blood= (ImageButton) findViewById(R.id.imageButton);
        take_blood= (ImageButton) findViewById(R.id.imageButton2);
        Others=(Button)findViewById(R.id.savelife);
    }

    public void donate_blood(View view){
        Intent intent= new Intent(getApplicationContext(),Donater.class);
        startActivity(intent);
    }

    public void need_blood(View view){
        Intent intent= new Intent(getApplicationContext(),NeedyOfBlood.class);
        startActivity(intent);
    }
    public void Lifesave(View view){
        Intent intent=new Intent(getApplicationContext(),LifeSaverList.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) {
            Intent intent=new Intent(getApplicationContext(),SigninPage.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}