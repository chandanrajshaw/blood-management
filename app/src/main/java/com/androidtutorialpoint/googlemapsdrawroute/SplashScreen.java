package com.androidtutorialpoint.googlemapsdrawroute;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    public static String mysharedprefrences;
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        sharedpreferences = getSharedPreferences(mysharedprefrences, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (sharedpreferences.getBoolean("Registered", false)) {
                    Intent mainIntent = new Intent(SplashScreen.this, HomePage.class);
                    startActivity(mainIntent);
                    finish();
                } else {
                    Intent mainIntent = new Intent(SplashScreen.this, SigninPage.class);
                    startActivity(mainIntent);
                    finish();
                }
            }

        }, SPLASH_TIME_OUT);
    }
}
