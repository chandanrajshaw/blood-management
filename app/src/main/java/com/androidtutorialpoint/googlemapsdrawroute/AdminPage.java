package com.androidtutorialpoint.googlemapsdrawroute;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AdminPage extends AppCompatActivity {
    Button btn_donor, btn_other;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_page);
        btn_donor = (Button) findViewById(R.id.button5);
        btn_other = (Button) findViewById(R.id.othr);
    }
    public void donorList(View view) {
        // Toast.makeText(this,"hey this is clicked",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, DonorList.class);
        startActivity(intent);
    }
    public void Other(View view) {
        Intent intent = new Intent(this, OthersList.class);
        startActivity(intent);
    }
}