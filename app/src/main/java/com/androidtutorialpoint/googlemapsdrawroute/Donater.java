package com.androidtutorialpoint.googlemapsdrawroute;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Donater extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText editText_name, editText_city, editText_contact, editText_address;
    ArrayAdapter adapter_gender, adapter_age;
    Spinner spinner_bgroup, spinner_agegroup;
    String selected_gender, selected_age;
    String name, city, contact, address;
    DBFacultyClassAdapter dataBaseClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donater);

        editText_name = (EditText) findViewById(R.id.editText);
        editText_city = (EditText) findViewById(R.id.editText2);
        editText_contact = (EditText) findViewById(R.id.editText3);
        editText_address = (EditText) findViewById(R.id.editText4);
        spinner_bgroup = (Spinner) findViewById(R.id.spinner);
        spinner_agegroup = (Spinner) findViewById(R.id.spinner2);
        dataBaseClass = new DBFacultyClassAdapter(this);
        adapter_gender = ArrayAdapter.createFromResource(this, R.array.Blood_Group, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_gender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner_bgroup.setAdapter(adapter_gender);
        //Array adapter for age spinner class
        adapter_age = ArrayAdapter.createFromResource(this,
                R.array.Age_Group, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_age.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_agegroup.setAdapter(adapter_age);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String label = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        // Toast.makeText(parent.getContext(), "You selected: " + label,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void save(View view) {
        selected_gender = spinner_bgroup.getSelectedItem().toString();
        selected_age = spinner_agegroup.getSelectedItem().toString();

        name = editText_name.getText().toString().trim();
        if (!StringUtils.isValidString(name)) {
            Toast.makeText(getApplicationContext(), "Enter valid name", Toast.LENGTH_LONG).show();
            return;
        }
        city = editText_city.getText().toString().trim();
        if (!StringUtils.isValidString(city)) {
            Toast.makeText(getApplicationContext(), "Enter city name", Toast.LENGTH_LONG).show();
            return;
        }
        contact = editText_contact.getText().toString().trim();
        if (!StringUtils.isValidString(contact)) {
            Toast.makeText(getApplicationContext(), "Enter  password", Toast.LENGTH_LONG).show();
            return;
        }

        address = editText_address.getText().toString().trim();
        if (!StringUtils.isValidString(address)) {
            Toast.makeText(getApplicationContext(), "Enter  cpwd", Toast.LENGTH_LONG).show();
            return;
        }


        //  Toast.makeText(this, "You selected: " + name+city+contact+address+selected_gender+selected_gender+selected_age, Toast.LENGTH_LONG).show();


        long id = dataBaseClass.insertData(name, city, contact, selected_gender, selected_age, address);
        if (id < 0) {
            Message.message(this, "unsuccessfull");
        } else {
            Message.message(this, "successfully insert a row");
            Intent intent = new Intent(getApplicationContext(), HomePage.class);
            startActivity(intent);
            finish();

        }
    }

    public void update(View view) {
        name = editText_name.getText().toString().trim();
        if (!StringUtils.isValidString(name)) {
            Toast.makeText(getApplicationContext(), "Enter valid name", Toast.LENGTH_LONG).show();
            return;
        }
        city = editText_city.getText().toString().trim();
        if (!StringUtils.isValidString(city)) {
            Toast.makeText(getApplicationContext(), "Enter city name", Toast.LENGTH_LONG).show();
            return;
        }
        contact = editText_contact.getText().toString().trim();
        if (!StringUtils.isValidString(contact)) {
            Toast.makeText(getApplicationContext(), "Enter  password", Toast.LENGTH_LONG).show();
            return;
        }

        address = editText_address.getText().toString().trim();
        if (!StringUtils.isValidString(address)) {
            Toast.makeText(getApplicationContext(), "Enter  cpwd", Toast.LENGTH_LONG).show();
            return;
        }

        //updating detail
        int update = dataBaseClass.updateData(name, city, contact, address);

        if (update >= 0) {
            Toast.makeText(getApplicationContext(), "Data updated", Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(getApplicationContext(), "Data not updated", Toast.LENGTH_LONG).show();

        }


    }
}




