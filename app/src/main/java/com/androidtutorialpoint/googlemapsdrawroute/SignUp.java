package com.androidtutorialpoint.googlemapsdrawroute;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
public class SignUp extends AppCompatActivity {
    EditText editText_fullname,editText_password,editText_cpassword;
    Button button_reg;
    DataBaseClassAdapter dataBaseClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        editText_fullname = (EditText) findViewById(R.id.editText5);
        editText_password = (EditText) findViewById(R.id.editText9);
        editText_cpassword = (EditText) findViewById(R.id.editText10);
        button_reg = (Button) findViewById(R.id.button);
        dataBaseClass = new DataBaseClassAdapter(this);
    }
    public  void register(View view)
    {
        String fullname= editText_fullname.getText().toString();
        if (!StringUtils.isValidString(fullname)) {
            Toast.makeText(getApplicationContext(), "Enter valid name", Toast.LENGTH_LONG).show();
            return;
        }

        String password= editText_password.getText().toString();
        if (!StringUtils.isValidString(password)) {
            Toast.makeText(getApplicationContext(), "Enter a valid password", Toast.LENGTH_LONG).show();
            return;
        }
        String cpassword= editText_cpassword.getText().toString();
        if (!StringUtils.isValidString(cpassword)) {
            Toast.makeText(getApplicationContext(), "Enter a valid cpwd", Toast.LENGTH_LONG).show();
            return;
        }
        if (!password.equals(cpassword)) {
            Toast.makeText(getApplicationContext(), "confirm password and password are not matching", Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(this,"hi",Toast.LENGTH_LONG);
        long id= dataBaseClass.insert(fullname,password,cpassword) ;
        if (id<0){
            Message.message(this,"unsuccessfull");
        }
        else {
            SplashScreen.editor.putBoolean("Registered",true);
            SplashScreen.editor.commit();
            Message.message(this,"successfully inserted a row");
            Intent intent=new Intent(getApplicationContext(),SigninPage.class);
            startActivity(intent);
            finish();
        }

    }
}
