package com.androidtutorialpoint.googlemapsdrawroute;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ProfileActivity extends AppCompatActivity {

    private static String imageEncoded;
    ImageView img_profile;
    EditText edt_Fname, edt_Lname, edt_currentPwd, edt_pwd, edt_reenterpwd;
    Button btn_update;
    private String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    String pasword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("Register", Context.MODE_PRIVATE);
        pasword = sharedPref.getString("pwd", "pwd");

        img_profile = (ImageView) findViewById(R.id.img_profile);
        edt_Fname = (EditText) findViewById(R.id.edt_Fname);
        edt_Lname = (EditText) findViewById(R.id.edt_Lname);
        edt_currentPwd = (EditText) findViewById(R.id.edt_currentPwd);
        edt_pwd = (EditText) findViewById(R.id.edt_pwd);
        edt_reenterpwd = (EditText) findViewById(R.id.edt_reenterpwd);
        btn_update = (Button) findViewById(R.id.btn_update);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Fname = edt_Fname.getText().toString().trim();
                String Lname = edt_Lname.getText().toString().trim();
                String current_pwd = edt_currentPwd.getText().toString().trim();
                String pwd = edt_pwd.getText().toString().trim();
                String re_pwd = edt_reenterpwd.getText().toString().trim();
                if (!Fname.equals("")) {
                    Toast.makeText(getApplicationContext(), "Should not be Empty", Toast.LENGTH_SHORT).show();
                } else if (!Lname.equals("")) {
                    Toast.makeText(getApplicationContext(), "Should not be Empty", Toast.LENGTH_SHORT).show();
                } else if (!current_pwd.equals("")) {
                    Toast.makeText(getApplicationContext(), "Should not be Empty", Toast.LENGTH_SHORT).show();
                } else if (!pwd.equals("")) {
                    Toast.makeText(getApplicationContext(), "Should not be Empty", Toast.LENGTH_SHORT).show();
                } else if (!re_pwd.equals("")) {
                    Toast.makeText(getApplicationContext(), "Should not be Empty", Toast.LENGTH_SHORT).show();
                } else if (pwd.equals(re_pwd) & pasword.equals(current_pwd)) {
//                    Toast.makeText(getApplicationContext(), "Should not be Empty", Toast.LENGTH_SHORT).show();
                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("Register", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("Fname", Fname);
                    editor.putString("Lname", Lname);
                    editor.putString("pwd", pwd);
                    editor.putString("imageEncoded", imageEncoded);
                    editor.commit();
                    Intent i = new Intent(ProfileActivity.this, SearchActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), "Should not be Empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(ProfileActivity.this);


                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Bitmap circleBitmap = Bitmap.createBitmap(bm.getWidth(), bm.getHeight(), Bitmap.Config.ARGB_8888);

        BitmapShader shader = new BitmapShader(bm, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setShader(shader);

        Canvas c = new Canvas(circleBitmap);
        c.drawCircle(bm.getWidth() / 2, bm.getHeight() / 2, bm.getWidth() / 2, paint);

        img_profile.setImageBitmap(circleBitmap);
        BitMapToString(bm);

//        img_profile.setImageBitmap(bm);
    }


    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        img_profile.setImageBitmap(thumbnail);

        Bitmap circleBitmap = Bitmap.createBitmap(thumbnail.getWidth(), thumbnail.getHeight(), Bitmap.Config.ARGB_8888);

        BitmapShader shader = new BitmapShader(thumbnail, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setShader(shader);

        Canvas c = new Canvas(circleBitmap);
        c.drawCircle(thumbnail.getWidth() / 2, thumbnail.getHeight() / 2, thumbnail.getWidth() / 2, paint);

        img_profile.setImageBitmap(circleBitmap);

        BitMapToString(thumbnail);

    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }
}
